#include <stdio.h>

//struct definition
struct student{
	char name[20];
	char subject[20];
	float marks;
};
int main(){
	//defining the struct array variable
	struct student students[5];
	//loop to get inputs from user
	for (int i = 0; i < 5; i++){
		printf("Enter Name of Student:\n");
		scanf("%s",students[i].name);
		printf("Enter the subject:\n");
		scanf("%s",students[i].subject);
		printf("\nEnter marks for the subject:\n");
		scanf("%f",&students[i].marks);
	}
	//loop to print the details
	for(int i = 0; i < 5; i++){
		printf("%d. Name: %s Subject: %s marks: %.2f\n",i+1,students[i].name,students[i].subject,students[i].marks);
	}
}
